class Employee {
  constructor (name,age,salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name () {
    return this._name;
  }
  get age () {
    return this._age
  }

  get salary () {
    return this._salary
  }

  set salary (value) {
    this._salary = value;
  }

  set name (value) {
    this._name = value;
  }

  set age (value) {
    this._age = value;
  }
}

class Programmer extends Employee {
  constructor (name,age,salary, lang ) {
    super(name,age,salary);
    this._lang = lang;
  }

  set salary (value) {
    this._salary = value;
  }

  get salary () {
    return this._salary * 3;
  }
  
}

let student = new Programmer ('Evgeniy',31,20000, 'UA');
let teacher = new Programmer ('Sergey',30,25000, 'UA');
console.log(student);
console.log(teacher);
